# RenameShowEpisodes

A small Rust program that renames and moves your ripped TV show episodes.

## Features

- Supports variable season length.
- Renames episodes like "S01E01". Which is the default for Plex/Jellyfin.
- Moves episodes to the TV show root folder.
- BLAZINGLY FAST!! Thanks to Rust x)

## How to use

Download the Windows
version [here](https://gitlab.com/uranderu/rename-show-episodes/-/raw/main/bin/rename_show_episodes_windows.exe?inline=false)
or the Linux
version [here](https://gitlab.com/uranderu/rename-show-episodes/-/raw/main/bin/rename_show_episodes_linux?inline=false).
After that just run the program and it'll show instructions where necessary.
