use std::env;
use std::fs;
use std::io::{stdin, stdout, Write};
use std::path::{Path, PathBuf};

struct RenameShowEpisodes {
    episodes: Vec<PathBuf>,
    new_episode_names: Vec<String>,
}

impl RenameShowEpisodes {
    fn format_int(number: i32) -> String {
        // Prefixes 0 to any int below 10 and adds 1 so that S00 becomes S01 etc.
        format!("{:02}", number + 1)
    }

    fn find_all_episodes(&mut self) {
        let show_location = Self::get_input("Please give the full path to the show's folder.");
        env::set_current_dir(&show_location).unwrap();

        for entry in fs::read_dir(&show_location).unwrap() {
            let path = entry.unwrap().path();
            if path.is_file() {
                self.episodes.push(path.to_owned());
            }
        }

        self.episodes.sort();  // Order is not guaranteed.
    }

    fn generate_episode_names(&mut self) {
        let season_amount = Self::get_input("How many seasons does the show have?").parse().unwrap();

        let equal_episode_amount = Self::get_input("Does every season have the same amount of episodes? yes/no");
        if equal_episode_amount == "yes" {
            let episode_amount: i32 = Self::get_input("How many episodes does every season have?").parse().unwrap();
            for season in 0..season_amount {
                for episode in 0..episode_amount {
                    self.new_episode_names.push(format!("S{}E{}", Self::format_int(season), Self::format_int(episode)));
                }
            }
        } else {
            let mut episodes_per_season = vec![];
            for season in 0..season_amount {
                let episode_amount: i32 = Self::get_input(&format!("How many episodes does season {} have?", Self::format_int(season))).parse().unwrap();
                episodes_per_season.push(episode_amount);
            }

            for season in 0..season_amount {
                for episode in 0..episodes_per_season[season as usize] {
                    self.new_episode_names.push(format!("S{}E{}", Self::format_int(season), Self::format_int(episode)));
                }
            }
        }
    }

    fn rename_and_move_episodes(&self) {
        for (index, episode) in self.episodes.iter().enumerate() {
            let file_extension = episode.extension().unwrap().to_str().unwrap();
            let new_name = format!("{}{}{}", self.new_episode_names[index], ".", file_extension);
            fs::rename(episode, Path::new(&new_name)).unwrap();
        }
    }

    fn get_input(prompt: &str) -> String {
        println!("{}", prompt);
        stdout().flush().unwrap();
        let mut input = String::new();
        stdin().read_line(&mut input).unwrap();
        input.trim().to_owned()
    }

    fn new() -> Self {
        println!("WARNING: Please remove all non video files from the show's folder.");

        let mut obj = Self {
            episodes: vec![],
            new_episode_names: vec![],
        };
        obj.find_all_episodes();
        obj.generate_episode_names();
        obj.rename_and_move_episodes();

        println!("Renaming and moving of episodes complete.");
        obj
    }
}

fn main() {
    RenameShowEpisodes::new();
}
